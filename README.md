This is a minimal reproduction of a bug with fast refresh when using styled-components in a next.js app. This
bug causes changes to the styles of components in certain situations to not be applied during a fast refresh, despite
the fact the module being changed is reloaded.

This repo is based off of the [current example](https://github.com/vercel/next.js/tree/canary/examples/with-styled-components) 
for using next.js with styled-components, and the `_app.jsx` and `_document.jsx` files are identical to the example, as well
as the babel config. This bug was introduced in next.js `9.3.7-canary.19` (not reproducible in `9.3.7-canary.18`), which
leads me to believe that it is an issue with Fast Refresh. It is reproducible on the current latest version of next as well.

Styled components can "wrap" other styled components to create extensions of their styles. This doesn't actually create
a component tree that contains a wrapper component and a wrapped component; instead, styled-components creates a new
component that duplicates the old component, but with the new styles added. When one of the "wrapped" components changes,
the changes are not properly updated if only the "wrapper" is actually being used in component tree.

For example, running in dev mode and changing the background of `StyledThing` in `components.js` will not properly update
on the webpage. In the browser console, we can see that `components.js` is being reloaded and re-evaluated, but 
`index.jsx` is never reloaded, and so the updated component is never re-wrapped into `Wrapper` in `index.jsx` and inserted
into the React tree. If anything that _isn't_ a React component is exported by `components.js`, then this issue no longer
exists. (However, multiple react components can be exported with the same issue).

My guess is that roughly the following logic is being applied to cause the bug:
 - Fast Refresh is noticing that `components.js` needs to be reloaded, grabs the new file and re-evaluates it.
 - It sees that the only things exported by this file are react components, so it'll attempt to update only parts of
 the existing React tree that are using these components.
 - Since `StyledThing` doesn't appear anywhere in the React tree, nothing is updated and `index.jsx` is not re-run, so
 the changes are never applied.

However, when something else (like a number) is exported from `components.js`, the following happens:
 - Fast Refresh is noticing that `components.js` needs to be reloaded, grabs the new file and re-evaluates it.
 - At least one non-React-component thing is exported, which means it may have effects on files that import it beyond
 just changes to the React tree. 
 - To play it safe, Fast Refresh falls back to the more "traditional" HMR behavior of re-evaluating all modules that 
 import `components.js`, which includes `index.jsx`.

The mistake is in the assumption that the only effect a React component can have on a program is if it is inserted into
the component tree, and can be entirely ignored if not used in that way. While a mostly reasonable assumption, this breaks
in situations like styled-components wrappers where components are used indirectly, having an effect on the program execution
without ever being inserted into the tree.
 
import React from 'react';
import styled from 'styled-components';

import { StyledThing } from '../lib/components';

console.log('evaluate index.jsx');

const Wrapper = styled(StyledThing)``;

export function IndexPage() {
  return <Wrapper>Hello, World!</Wrapper>;
}

export default IndexPage;
